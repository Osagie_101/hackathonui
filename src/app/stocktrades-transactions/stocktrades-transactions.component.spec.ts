import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StocktradesTransactionsComponent } from './stocktrades-transactions.component';

describe('StocktradesTransactionsComponent', () => {
  let component: StocktradesTransactionsComponent;
  let fixture: ComponentFixture<StocktradesTransactionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StocktradesTransactionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StocktradesTransactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
