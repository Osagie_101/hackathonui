import { Component, OnInit, Input } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";
import { ActivatedRoute, Router } from '@angular/router';
import {Stocktrades} from '../shared/stocktrades';

@Component({
  selector: 'app-stocktrades-transactions',
  templateUrl: './stocktrades-transactions.component.html',
  styleUrls: ['./stocktrades-transactions.component.css']
})
export class StocktradesTransactionsComponent implements OnInit {

  @Input('ngModel') stocktrades = new Stocktrades();

  once: number = 0;
  prices: number[] = [];
  change: number[] = [];
  tickers: string[] = ["AAPL", "C", "GOOGL", "TSLA", "FB", "AMZN"];


  constructor(
    public restApi: RestApiService,
    public actRoute: ActivatedRoute,
    public router: Router
  ) { }

  ngOnInit(){
    this.loadStocks()
  }

  loadStocks(){
    let i: number = 0;
    let arr = ["AAPL", "C", "GOOGL", "TSLA", "FB", "AMZN"];
    for(let ticker in arr){
      console.log(this.tickers[ticker]);
      this.restApi.getPriceYes(this.tickers[ticker]).subscribe((data:number[]) => {
        this.prices[ticker] = data[1];
        this.change[ticker] = data[1]/data[0] - 1;
      })
      i+=1;
    }
    console.log(this.prices)
    console.log(this.change)
  }

  createTransactionsBUY() {
    this.stocktrades.buyOrSell = "BUY";
    console.log(this.stocktrades);
    this.restApi.createTransactions(this.stocktrades).subscribe(data => {
      alert("you made a trade")
    })

  }

  createTransactionsSELL() {
    this.stocktrades.buyOrSell = "SELL";
    console.log(this.stocktrades);
    this.restApi.createTransactions(this.stocktrades).subscribe(data => {
      alert("you made a trade")
    })
  }

  getPrice(ticker:string) {
    this.restApi.getPriceOfStockIoana(ticker).subscribe(data => {
      this.stocktrades.price = data;
    })
  }

  totalValue(){
    return (this.stocktrades.price*this.stocktrades.volume).toFixed(2);
  }

  returnPrice(){
    if(this.stocktrades.stockTicker !== "" && this.stocktrades.price == 0 && this.once < 10){
      this.once +=1;
      this.getPrice(this.stocktrades.stockTicker);
    }

    return this.stocktrades.price.toFixed(2);
  }

}
