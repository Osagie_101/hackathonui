import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Stocktrades } from './stocktrades';
import { Stock } from './stock';
@Injectable({
  providedIn: 'root'
})
export class RestApiService {

  apiURL= environment.apiURL;
  apiFrank= environment.apiFrank;


  constructor(private http: HttpClient) { }

  //Http Options
  httpOptions = {
    headers: new HttpHeaders ( {
      'Content-Type': 'application/json'
    })
}

//HttpClient api get() method
getAllTrades(): Observable<Stocktrades[]> {
  return this.http.get<Stocktrades[]>(this.apiURL)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )

}

// HttpClient API get() method
getByTicker(ticker:string): Observable<Stocktrades[]> {
  return this.http.get<Stocktrades[]>(this.apiURL +"findByStockTicker/" + ticker)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
}

// HttpClient API post() method
createTransactions(stocktrade:Stocktrades): Observable<Stocktrades> {
  return this.http.post<Stocktrades>(this.apiURL,JSON.stringify(stocktrade),this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
}

//
getAllSharesOwned(ticker:string): Observable<number>{
  return this.http.get<number>(this.apiURL +"countBasketOf/" + ticker)
  .pipe(
    retry(1),
    catchError(this.handleError)
  )
}

getPnlByTicker(ticker:string):Observable<number>{
  return this.http.get<number>(this.apiURL +"pnl/" + ticker)
  .pipe(
    retry(1),
    catchError(this.handleError)
  )
}

getPriceOfStockIoana(ticker:string):Observable<number>{
  return this.http.get<number>(this.apiURL +"getCachedPriceData/" + ticker)
  .pipe(
    retry(1),
    catchError(this.handleError)
  )
}

getPriceYes(ticker:string):Observable<number[]>{
  console.log(ticker);
  return this.http.get<number[]>(this.apiURL +"getPriceYesterday/" + ticker)
  .pipe(
    retry(1),
    catchError(this.handleError)
  )
}

getOwnedStocks():Observable<string[]>{
  return this.http.get<string[]>(this.apiURL +"getOwnedTickers/" )
  .pipe(
    retry(1),
    catchError(this.handleError)
  )
}

// HttpClient API get() method
getStockByTicker(ticker:string): Observable<Stock> {
  return this.http.get<Stock>(this.apiURL +"getStockByTicker/" + ticker.toUpperCase())
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
}


getStocksAndPnl(tickers:string[]):Observable<Map<string, number>>{
  console.log( this.http.get<Map<string, number>>(this.apiURL +"getStocksAndPnl/" + tickers))
  return this.http.get<Map<string, number>>(this.apiURL +"getStocksAndPnl/" + tickers)
  .pipe(
    retry(1),
    catchError(this.handleError)
  )
}

getTotalShares(tickers:string[]):Observable<Map<string, number>>{
  return this.http.get<Map<string, number>>(this.apiURL +"totalSharesOwned/" + tickers)
  .pipe(
    retry(1),
    catchError(this.handleError)
  )
}


getShareValues(tickers:string[]):Observable<Map<string, number>>{
  return this.http.get<Map<string, number>>(this.apiURL +"getCurrentValueOfShares/" + tickers)
  .pipe(
    retry(1),
    catchError(this.handleError)
  )
}

// HttpClient API get() method 
getPriceData(ticker: string): Observable<any> {
  return this.http.get<any>(this.apiFrank + "?ticker=" + ticker + "&num_days=" + 10)
  // "https://3p7zu95yg3.execute-api.us-east-1.amazonaws.com/default/priceFeed2?ticker=TSLA&num_days=30"
  .pipe(
    retry(1),
    catchError(this.handleError)
  )
}



// Error handling
handleError(error:any) {
  let errorMessage = '';
  if(error.error instanceof ErrorEvent) {
    // Get client-side error
    errorMessage = error.error.message;
  } else {
    // Get server-side error
    errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
  }
  window.alert(errorMessage);
  return throwError(errorMessage);
}

}


