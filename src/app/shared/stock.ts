export class Stock {

    ticker: string;
    company: string;
    price: number;
    industry: string;
    advice: string;


    constructor(){
        this.ticker="";
        this.company="";
        this.price=0;
        this.industry="";
        this.advice="";
    }
}
