import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";
import { Stocktrades } from '../shared/stocktrades';
@Component({
  selector: 'app-stocktrades-history',
  templateUrl: './stocktrades-history.component.html',
  styleUrls: ['./stocktrades-history.component.css']
})
export class StocktradesHistoryComponent implements OnInit {

  stockTrades: Stocktrades[]=[];
  ownedStocks: string[]=[];
  chartData: any[] = [{ticker: "", pln: []}];
  totalShares:number=0;
  tickers:string[]=["AMZN", "AAPL", "C", "FB", "GOOGL", "TSLA"]
  stocksData: Map<string,any[]>= new Map();
  stockImages: {[id: string]: string} = {
    'GOOGL': "https://media-exp1.licdn.com/dms/image/C4D0BAQHiNSL4Or29cg/company-logo_200_200/0/1519856215226?e=2159024400&v=beta&t=r--a5-Dl4gvVE-xIkq8QyBzZ8mQ-OYwBOrixNzR95H0",
    'AAPL': 'https://www.marketbeat.com/logos/apple-inc-logo.png',
    'AMZN': 'https://thumbor.forbes.com/thumbor/fit-in/416x416/filters%3Aformat%28jpg%29/https%3A%2F%2Fspecials-images.forbesimg.com%2Fimageserve%2F5d825aa26de3150009a4616c%2F0x0.jpg%3Fbackground%3D000000%26cropX1%3D0%26cropX2%3D416%26cropY1%3D0%26cropY2%3D416',
    'C': 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Citi.svg/1200px-Citi.svg.png',
    'FB': 'https://i.pinimg.com/originals/c7/92/c1/c792c132eb020a6486f8d3755dcbe3dd.jpg',
    'TSLA': 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/bb/Tesla_T_symbol.svg/1200px-Tesla_T_symbol.svg.png'
  };

  constructor(public restApi: RestApiService) { }

  ngOnInit(): void {

    this.loadTransactions()
    this.getOwnedStocks()

  }

  loadTransactions() {
    return this.restApi.getAllTrades().subscribe((data:Stocktrades[] ) => {
      this.stockTrades = data;

    })
  }

  getOwnedStocks(){
    return this.restApi.getOwnedStocks().subscribe((data:string[] ) => {

      this.ownedStocks = data;
      this.getStocksAndPnl(this.ownedStocks);
      this.getTotalShares(this.ownedStocks);
      this.getShareValues(this.ownedStocks);

    })

  }





  getStocksAndPnl(ticker:string[]){
    return this.restApi.getStocksAndPnl(ticker).subscribe((data:Map<string, number>) => {
      console.log(data);
      for(let[key, value] of Object.entries(data)){
        let currentData:any[]=[0,0,0,0];
        currentData=this.stocksData.get(key) || [0,0,0,0];
        currentData[0]=value;
         this.stocksData.set(key, currentData);
      }

    })
  }



getTotalShares(ticker: string[]){
    return this.restApi.getTotalShares(ticker).subscribe((data:Map<string, number>) => {
      for(let[key, value] of Object.entries(data)){
        let currentData:any[]=[0,0,0,0];
        currentData=this.stocksData.get(key) || [0,0,0,0];
        currentData[1]=value;
         this.stocksData.set(key, currentData);
      }
    })
  }


  getShareValues(ticker: string[]){
    return this.restApi.getShareValues(ticker).subscribe((data:Map<string, number>) => {
      for(let[key, value] of Object.entries(data)){
        let currentData:any[]=[0,0,0,0];
        currentData=this.stocksData.get(key) || [0,0,0,0];
        currentData[2]=value;
        currentData[3]= this.stockImages[key];
         this.stocksData.set(key, currentData);
      }
    })
  }


}


