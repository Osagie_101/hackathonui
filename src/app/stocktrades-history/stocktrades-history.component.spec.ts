import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StocktradesHistoryComponent } from './stocktrades-history.component';

describe('StocktradesHistoryComponent', () => {
  let component: StocktradesHistoryComponent;
  let fixture: ComponentFixture<StocktradesHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StocktradesHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StocktradesHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
