import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StocktradesHistoryComponent } from './stocktrades-history/stocktrades-history.component';
import { StocktradesTransactionsComponent } from './stocktrades-transactions/stocktrades-transactions.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'stocktrades-history' },
  { path: 'stocktrades-history', component: StocktradesHistoryComponent },
  { path: 'stocktrades-transactions', component: StocktradesTransactionsComponent },
  { path: '**', component: StocktradesHistoryComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
