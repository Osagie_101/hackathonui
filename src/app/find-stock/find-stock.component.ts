import { Component, OnInit, Input} from '@angular/core';
import { RestApiService } from '../shared/rest-api.service';
import { Stock } from '../shared/stock';

@Component({
  selector: 'app-find-stock',
  templateUrl: './find-stock.component.html',
  styleUrls: ['./find-stock.component.css']
})
export class FindStockComponent implements OnInit {

  stock: any={};
  ticker = '';
  isShown: boolean=false;
  count:number=0;

  constructor(public restApi: RestApiService) { }

  ngOnInit(): void {
    this.isShown = false; //hidden every time subscribe detects change
    this.count=0;
  }

  findStock(){
    // this.ticker = ticker;
    return this.restApi.getStockByTicker(this.ticker).subscribe(data => {
      this.stock = data;
      console.log(data)
    })
  }

  toggleShow() {
   

    if(this.count ==0){
      this.isShown = ! this.isShown;
      this.count +=1;
    }
    
  }
  
  title = 'simple-chart';
  chartData: any[] = [{name: "", series: []}];
  view: [number, number] = [1000, 300];

  // options
  legend: boolean = true;
  showLabels: boolean = true;
  animations: boolean = true;
  xAxis: boolean = true;
  yAxis: boolean = true;
  showYAxisLabel: boolean = true;
  showXAxisLabel: boolean = true;
  xAxisLabel: string = 'Date';
  yAxisLabel: string = 'Price $';
  timeline: boolean = true;


  colorScheme = {
    domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5']
  };

  

  loadPriceData() {
    this.restApi.getPriceData(this.ticker).subscribe((data: any) => {
        console.log(data);
        this.chartData = [{name: data['ticker'],
                           series: data['price_data']}];

        console.log("This is the data" + this.chartData);
    })
  }

  onSelect(data:any): void {
    console.log('Item clicked', JSON.parse(JSON.stringify(data)));
  }

  onActivate(data:any): void {
    console.log('Activate', JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data:any): void {
    console.log('Deactivate', JSON.parse(JSON.stringify(data)));
  }

}
